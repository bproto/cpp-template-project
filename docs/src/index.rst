####################
C++ Template Project
####################

This is a documentation for all available components.

****

.. WORK MANAGEMENT SECTION ===================================================

.. Hidden toctree to manage the sidebar navigation. Match the contents list below.

.. toctree::
   :maxdepth: 1
   :caption: api
   :hidden:

   common/index.rst
   shapes/index.rst
   vcs/index.rst

.. _part-work:

API Documentation
=================

- :doc:`common/index`
- :doc:`shapes/index`
- :doc:`vcs/index`

.. -------------------

.. _part-guides:
