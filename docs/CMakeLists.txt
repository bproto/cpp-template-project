# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Copyright 2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

option(ENABLE_DOCS "Enable Documentation" ON)
if(NOT ENABLE_DOCS)

  macro(add_api_docs)
  endmacro()

  macro(add_docs)
  endmacro()

  macro(generate_docs)
  endmacro()

  return()

endif()

#------------------------------------------------------------------------------#
# Breathe + Doxygen + Exhale                                                   #
#------------------------------------------------------------------------------#

# This is a "meta" target that is used to collect all C++ information.
add_custom_target(api-docs-info)

set(DOXYGEN_TAGFILE "cppreference-doxygen-web-tag.xml=http://en.cppreference.com/w/")
set_target_properties(api-docs-info
  PROPERTIES
    BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}"
    SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}"
    TAGFILE "${CMAKE_CURRENT_SOURCE_DIR}/doxygen/${DOXYGEN_TAGFILE}"
)

function(get_docs_property _prop)

  set(output "${_prop}_var")

  get_target_property("${output}" api-docs-info "${_prop}")
  list(JOIN "${output}" ",\n    " "${output}")

  if("${${output}}" STREQUAL "${output}-NOTFOUND")
    message(FATAL_ERROR "Unable to get '${_prop}' property from 'api-docs-info' target.")
  endif()

  set("${_prop}" "${${output}}" PARENT_SCOPE)

endfunction()

function(add_api_docs _target _sources)

  get_docs_property(BINARY_DIR)

  set_property(
    TARGET
      api-docs-info
    APPEND
      PROPERTY
        BREATHE_PROJECTS "'${_target}': '${BINARY_DIR}/doxygen/${_target}/xml'"
  )
  set_property(
    TARGET
      api-docs-info
    APPEND
      PROPERTY
        BREATHE_PROJECTS_SOURCE "'${_target}': '${_sources}'"
  )

  set(args "'${_target}': {\n\
        'containmentFolder': '${BINARY_DIR}/src/${_target}',\n\
        'doxygenStripFromPath': '${_sources}'\n\
    }"
  )
  set_property(
    TARGET
      api-docs-info
    APPEND
      PROPERTY
        EXHALE_PROJECTS_ARGS ${args}
  )

endfunction()

#------------------------------------------------------------------------------#
# Sphinx                                                                       #
#------------------------------------------------------------------------------#

# This is a "meta" target that is used to collect all documentation files.
add_custom_target(sphinx-docs)

macro(add_docs _target _dir)

  set(copy_target "copy-${_target}-rst-docs")

  #[[=
  Copy rst files to build directory before generating the html documentation.
  Some of the rst files are generated, so they only exist in the build directory.
  Sphinx needs all files in the same directory in order to genrate the html, so
  we need to copy all the non-gnerated rst files from the source to the build
  directory before we run sphinx.
  #=]]

  get_docs_property(BINARY_DIR)
  message("${CMAKE_COMMAND} -E copy_directory ${_dir} ${BINARY_DIR}/src/${_target}")

  add_custom_target(${copy_target}
    COMMAND
      "${CMAKE_COMMAND}" -E copy_directory "${_dir}" "${BINARY_DIR}/src/${_target}"
    COMMENT
      "Copying documentation of the '${_target}' target to the build directory."
  )

  add_dependencies(sphinx-docs ${copy_target})

endmacro()

macro(add_main_docs _src _dest)

  set(copy_target "copy-rst-docs")
  add_custom_target(${copy_target}
    COMMAND
      "${CMAKE_COMMAND}" -E copy_directory "${_src}" "${_dest}"
    COMMENT
      "Copying main documentation to the build directory."
  )

  add_dependencies(sphinx-docs ${copy_target})

endmacro()

#------------------------------------------------------------------------------#
# Sphinx - Generation                                                          #
#------------------------------------------------------------------------------#

set_property(DIRECTORY
  APPEND
    PROPERTY
      # When "clean" target is run,
      ADDITIONAL_MAKE_CLEAN_FILES
        # remove the Doxygen build directory.
        "${CMAKE_CURRENT_BINARY_DIR}/doxygen"
        # remove the Sphinx build directory.
        "${CMAKE_CURRENT_BINARY_DIR}/html"
        # remove the doctrees build directory.
        "${CMAKE_CURRENT_BINARY_DIR}/sphinx/_doctrees"
        # remove the Breathe build directory.
        "${CMAKE_CURRENT_BINARY_DIR}/sphinx/breathe"
        # remove the src build directory.
        "${CMAKE_CURRENT_BINARY_DIR}/src"
)

function(generate_conf_py _intput_dir _output_dir)

  get_docs_property(BREATHE_PROJECTS)
  get_docs_property(BREATHE_PROJECTS_SOURCE)
  get_docs_property(EXHALE_PROJECTS_ARGS)
  get_docs_property(TAGFILE)

  set(output_file "${_output_dir}/conf.py")

  configure_file("${_intput_dir}/conf.py.in" "${output_file}" @ONLY)

  file(READ "${_intput_dir}/exhale_multiproject_monkeypatch.py" patch)
  file(APPEND "${output_file}" "\n${patch}")

endfunction()

function(generate_docs)

  get_docs_property(BINARY_DIR)
  get_docs_property(SOURCE_DIR)

  set(sphinx_dir "${BINARY_DIR}/sphinx")
  generate_conf_py("${SOURCE_DIR}/sphinx" "${sphinx_dir}")

  add_main_docs("${SOURCE_DIR}/src" "${BINARY_DIR}/src")

  list(APPEND CMAKE_MODULE_PATH "${SOURCE_DIR}/cmake")
  find_package(Sphinx REQUIRED breathe exhale)

  add_custom_target(docs
    COMMAND
      ${SPHINX_BUILD_EXECUTABLE}
        -b html
        -c "${sphinx_dir}"
        -d "${sphinx_dir}/_doctrees"
        -j auto
        "${BINARY_DIR}/src"
        "${BINARY_DIR}/html"
    DEPENDS
      sphinx-docs
    VERBATIM
    WORKING_DIRECTORY
      "${sphinx_dir}"
    COMMENT
      "Generating documentation with Sphinx"
  )

endfunction()
