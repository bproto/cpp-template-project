# Copyright 2020-2021 Zinchenko Serhii <zinchenko.serhii@pm.me>.
#
# Cpp Template Project: A template CMake project to get you started with
# C++ and tooling.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(vcs
  LANGUAGES
    CXX
  DESCRIPTION
    "Simple abstraction for a VCS."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(API_HEADERS
  accessor.hpp
  read_only_vcs_info.hpp
)

list(TRANSFORM API_HEADERS PREPEND "include/vcs/")
set(API_HEADERS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

set(HEADERS
  ${API_HEADERS}

  src/accessor_impl.hpp
  src/read_only_git_info_impl.hpp
)

set(SOURCE
  src/accessor_impl.cpp
  src/read_only_git_info_impl.cpp
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" ${HEADERS} ${SOURCE})
add_library(Ctp::Vcs ALIAS ${PROJECT_NAME})

source_group("api" FILES ${API_HEADERS})

target_init("${PROJECT_NAME}")

add_api_docs("${PROJECT_NAME}" "${API_HEADERS_DIR}")
add_docs("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  PUBLIC
    $<BUILD_INTERFACE:${API_HEADERS_DIR}>
    $<INSTALL_INTERFACE:include>
  PRIVATE
    "${PROJECT_SOURCE_DIR}"
)

#------------------------------------------------------------------------------#
# Project precompile headers                                                   #
#------------------------------------------------------------------------------#

target_precompile_headers("${PROJECT_NAME}"
  PRIVATE
    <boost/noncopyable.hpp>
    <memory>
    <string>
    <utility>
)

#------------------------------------------------------------------------------#
# Project libraries                                                            #
#------------------------------------------------------------------------------#

target_link_libraries("${PROJECT_NAME}"
  PUBLIC
    Boost::boost
)

#------------------------------------------------------------------------------#
# Git watcher                                                                  #
#------------------------------------------------------------------------------#

# Define the two required variables before including the source code for watching
# a git repository.
set(PRE_CONFIGURE_FILE "src/read_only_git_info_impl.cpp.in")
set(POST_CONFIGURE_FILE "src/read_only_git_info_impl.cpp")

include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/GitWatcher.cmake)
